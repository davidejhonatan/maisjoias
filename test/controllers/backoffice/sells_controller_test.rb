require 'test_helper'

class Backoffice::SellsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @backoffice_sell = backoffice_sells(:one)
  end

  test "should get index" do
    get backoffice_sells_url
    assert_response :success
  end

  test "should get new" do
    get new_backoffice_sell_url
    assert_response :success
  end

  test "should create backoffice_sell" do
    assert_difference('Backoffice::Sell.count') do
      post backoffice_sells_url, params: { backoffice_sell: { client_id: @backoffice_sell.client_id, code: @backoffice_sell.code, description: @backoffice_sell.description, value_cents: @backoffice_sell.value_cents } }
    end

    assert_redirected_to backoffice_sell_url(Backoffice::Sell.last)
  end

  test "should show backoffice_sell" do
    get backoffice_sell_url(@backoffice_sell)
    assert_response :success
  end

  test "should get edit" do
    get edit_backoffice_sell_url(@backoffice_sell)
    assert_response :success
  end

  test "should update backoffice_sell" do
    patch backoffice_sell_url(@backoffice_sell), params: { backoffice_sell: { client_id: @backoffice_sell.client_id, code: @backoffice_sell.code, description: @backoffice_sell.description, value_cents: @backoffice_sell.value_cents } }
    assert_redirected_to backoffice_sell_url(@backoffice_sell)
  end

  test "should destroy backoffice_sell" do
    assert_difference('Backoffice::Sell.count', -1) do
      delete backoffice_sell_url(@backoffice_sell)
    end

    assert_redirected_to backoffice_sells_url
  end
end
