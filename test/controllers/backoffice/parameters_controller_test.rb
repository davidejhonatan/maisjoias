require 'test_helper'

class Backoffice::ParametersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @backoffice_parameter = backoffice_parameters(:one)
  end

  test "should get index" do
    get backoffice_parameters_url
    assert_response :success
  end

  test "should get new" do
    get new_backoffice_parameter_url
    assert_response :success
  end

  test "should create backoffice_parameter" do
    assert_difference('Backoffice::Parameter.count') do
      post backoffice_parameters_url, params: { backoffice_parameter: {  } }
    end

    assert_redirected_to backoffice_parameter_url(Backoffice::Parameter.last)
  end

  test "should show backoffice_parameter" do
    get backoffice_parameter_url(@backoffice_parameter)
    assert_response :success
  end

  test "should get edit" do
    get edit_backoffice_parameter_url(@backoffice_parameter)
    assert_response :success
  end

  test "should update backoffice_parameter" do
    patch backoffice_parameter_url(@backoffice_parameter), params: { backoffice_parameter: {  } }
    assert_redirected_to backoffice_parameter_url(@backoffice_parameter)
  end

  test "should destroy backoffice_parameter" do
    assert_difference('Backoffice::Parameter.count', -1) do
      delete backoffice_parameter_url(@backoffice_parameter)
    end

    assert_redirected_to backoffice_parameters_url
  end
end
