require 'test_helper'

class Panel::ClientsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get panel_clients_index_url
    assert_response :success
  end

end
