4# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


puts "Cadastrando ADMIN e BRANCH padrão..."

Branch.create!(name: "MaisJoias Natal", city: "Natal", address: "Rua Exemplo", neighbohod: "Capim Macio", state: "RN", status: 0)
Branch.create!(name: "MaisJoias Currais Novos", city: "Natal", address: "Rua da Subida", neighbohod: "Centro", state: "RN", status: 0)
Admin.create!(email: "admin@admin.com", password: "123456", password_confirmation: "123456", name: "Davide",
			  cpf: "07517403494", rg: "002689292", status: 0, branch_id: "1")
Admin.create!(email: "raphael@maisjoias.com", password: "123456", password_confirmation: "123456", name: "Raphael",
			  cpf: "07517403494", rg: "002689292", status: 0, branch_id: "1")
puts "ADMIN e BRANCH padrão cadastrados com sucesso!"

puts "-----------------------------"

puts "Cadastrando BANCO padrão"
Bank.create!(bank_name: "Banco do Brasil", bank_agency: "987456", bank_account: "654887984654", bank_account_type: "Conta Corrente", bank_variance: "01")
Bank.create!(bank_name: "Caixa", bank_agency: "755624", bank_account: "3546878964654", bank_account_type: "Poupança", bank_variance: "51")
puts "BANCO padrão cadastrado com sucesso!"

puts "-----------------------------"

puts "Cadastrando CLIENTE padrão"
Client.create!(email: "cliente@cliente.com", password: "123456", password_confirmation: "123456", name: "Victor Hugo", cpf: "123456789", rg: "987654321", address: "Rua sem nome", neighbohod: "Bairro exemplo", country: "Brasil",
	           state: "RN", status: 0, total_points: "", available_points: "", total_reactive: "", postal_code: "59078163", city: "Natal",
	           total_points_rescue: "", branch_id: "1", birthdate: Date.today - Random.rand(9000), bank_id: 1)
Client.create!(email: "Zara", password: "123456", password_confirmation: "123456", name: "Zara", cpf: "192168075174", rg: "002689292", address: "Rua exemplo", neighbohod: "Bairro exemplo", country: "Brasil",
	           state: "RN", status: 0, total_points: "", available_points: "", total_reactive: "", postal_code: "59078160", city: "Natal",
	           total_points_rescue: "", branch_id: "1", birthdate: Date.today - Random.rand(9000), bank_id: 2, indicated: 1)
puts "CLIENTE padrão cadastrado com sucesso!"

puts "-----------------------------"

puts "Cadastrando TELEFONE ..."
Phone.create!(phone: "12345678", client_id: "1")
Phone.create!(phone: "44448888", client_id: "1")
Phone.create!(phone: "33335555", client_id: "1")
puts "TELEFONE padrão cadastrado com sucesso!"

puts "-----------------------------"

puts "Cadastrando VENDA padrão..."
Sell.create!(client_id: 1, value_cents: 5000, code: 1, description: "Primeira venda do cliente 1", sell_date: Date.today + Random.rand(90), sell_type: "formvendas", status: 0)
Sell.create!(client_id: 1, value_cents: 1000, code: 2, description: "Strjrtujrtjrt", sell_date: Date.today + Random.rand(90), sell_type: "formdev", status: 1)
Sell.create!(client_id: 2, value_cents: 2000, code: 3, description: "wertwertwert", sell_date: Date.today + Random.rand(90), sell_type: "formvendas", status: 0)
puts "VENDA padrão cadastrada com sucesso!"

puts "Cadastrando Parâmetros"
Parameter.create!(label: "value_nivel_1",value: "4",name: "Valor Nível 1", status: 0)
Parameter.create!(label: "value_nivel_2",value: "3",name: "Valor Nível 2", status: 0)
Parameter.create!(label: "value_nivel_3",value: "2",name: "Valor Nível 3", status: 0)
Parameter.create!(label: "value_nivel_4",value: "1",name: "Valor Nível 4", status: 0)
Parameter.create!(label: "points_to_money",value: "0.7",name: "Valor dos pontos em real", status: 0)
Parameter.create!(label: "points_to_product",value: "1",name: "Valor dos pontos em produtos", status: 0)


puts "cadastrando tipos"
Rescuetype.create!(label: 'Dinheiro',status: 0)
Rescuetype.create!(label: 'Produtos', status: 0)

puts "cadastrando resgates"
Rescuepoint.create!(client_id: 1,rescuetype_id:1, points: 200, date_rescue: "2017-12-06", rescued: 0, branch_id: 1)
Rescuepoint.create!(client_id: 1,rescuetype_id:2, points: 300, date_rescue: "2017-12-05", rescued: 0, branch_id: 1)
Rescuepoint.create!(client_id: 1,rescuetype_id:2, points: 500, date_rescue: "2017-12-06", rescued: 1, branch_id: 1)
Rescuepoint.create!(client_id: 1,rescuetype_id:1, points: 100, date_rescue: "2017-12-06", rescued: 1, branch_id: 1)
Rescuepoint.create!(client_id: 2,rescuetype_id:1, points: 1500, date_rescue: "2017-12-06", rescued: 0, branch_id: 1)

puts "Cadastrando Voucher exemplo"
Voucher.create!(code: "12345",rescuepoint_id: 3,status: 0,value: 300)
Voucher.create!(code: "123456",rescuepoint_id: 4,status: 1,value: 300)

puts "cadastrando arvore de relação"
Tree.create!(father: 1, child: 1, status: 0, level: 1)
Tree.create!(father: 1, child: 2, status: 0, level: 2)
Tree.create!(father: 2, child: 2, status: 0, level: 1)