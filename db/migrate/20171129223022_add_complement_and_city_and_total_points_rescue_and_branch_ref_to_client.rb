class AddComplementAndCityAndTotalPointsRescueAndBranchRefToClient < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :complement, :string
    add_column :clients, :city, :string
    add_column :clients, :total_points_rescue, :integer
    add_reference :clients, :branch, foreign_key: true
  end
end
