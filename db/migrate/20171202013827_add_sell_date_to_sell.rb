class AddSellDateToSell < ActiveRecord::Migration[5.1]
  def change
    add_column :sells, :sell_date, :date
  end
end
