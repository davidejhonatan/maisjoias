class CreateBanks < ActiveRecord::Migration[5.1]
  def change
    create_table :banks do |t|
      t.string :bank_name
      t.string :bank_agency
      t.string :bank_account_type
      t.string :bank_account
      t.string :bank_variance

      t.timestamps
    end
  end
end
