class AddSellTypeToSell < ActiveRecord::Migration[5.1]
  def change
    add_column :sells, :sell_type, :string
  end
end
