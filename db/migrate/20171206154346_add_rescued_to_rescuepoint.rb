class AddRescuedToRescuepoint < ActiveRecord::Migration[5.1]
  def change
    add_column :rescuepoints, :rescued, :integer
  end
end
