class CreateParameters < ActiveRecord::Migration[5.1]
  def change
    create_table :parameters do |t|
      t.string :label
      t.string :value
      t.integer :status

      t.timestamps
    end
  end
end
