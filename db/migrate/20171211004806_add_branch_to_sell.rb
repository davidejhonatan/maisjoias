class AddBranchToSell < ActiveRecord::Migration[5.1]
  def change
    add_reference :sells, :branch, foreign_key: true
  end
end
