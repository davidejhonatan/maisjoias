class AddNameAndRoleAndCpfAndRgAndStatusAndBranchRefToAdmin < ActiveRecord::Migration[5.1]
  def change
    add_column :admins, :name, :string
    add_column :admins, :role, :integer
    add_column :admins, :cpf, :string
    add_column :admins, :rg, :string
    add_column :admins, :status, :integer
    add_reference :admins, :branch, foreign_key: true
  end
end
