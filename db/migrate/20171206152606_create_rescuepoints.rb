class CreateRescuepoints < ActiveRecord::Migration[5.1]
  def change
    create_table :rescuepoints do |t|
      t.references :client, foreign_key: true
      t.references :rescuetype, foreign_key: true
      t.integer :points
      t.date :date_rescue

      t.timestamps
    end
  end
end
