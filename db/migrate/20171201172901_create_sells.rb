class CreateSells < ActiveRecord::Migration[5.1]
  def change
    create_table :sells do |t|
      t.references :client, foreign_key: true
      t.integer :value_cents
      t.integer :code
      t.string :description

      t.timestamps
    end
  end
end
