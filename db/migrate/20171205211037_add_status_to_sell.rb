class AddStatusToSell < ActiveRecord::Migration[5.1]
  def change
    add_column :sells, :status, :integer
  end
end
