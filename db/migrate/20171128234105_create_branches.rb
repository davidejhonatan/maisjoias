class CreateBranches < ActiveRecord::Migration[5.1]
  def change
    create_table :branches do |t|
      t.string :name
      t.string :city
      t.string :address
      t.string :neighbohod
      t.string :state
      t.integer :status

      t.timestamps
    end
  end
end
