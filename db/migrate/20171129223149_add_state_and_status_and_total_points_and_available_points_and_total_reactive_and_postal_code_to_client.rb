class AddStateAndStatusAndTotalPointsAndAvailablePointsAndTotalReactiveAndPostalCodeToClient < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :state, :string
    add_column :clients, :status, :integer
    add_column :clients, :total_points, :integer
    add_column :clients, :available_points, :integer
    add_column :clients, :total_reactive, :integer
    add_column :clients, :postal_code, :string
  end
end
