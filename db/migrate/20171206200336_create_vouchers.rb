class CreateVouchers < ActiveRecord::Migration[5.1]
  def change
    create_table :vouchers do |t|
      t.string :code
      t.references :rescuepoint, foreign_key: true
      t.integer :status
      t.integer :value

      t.timestamps
    end
  end
end
