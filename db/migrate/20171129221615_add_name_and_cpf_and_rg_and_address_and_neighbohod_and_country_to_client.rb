class AddNameAndCpfAndRgAndAddressAndNeighbohodAndCountryToClient < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :name, :string
    add_column :clients, :cpf, :string
    add_column :clients, :rg, :string
    add_column :clients, :address, :string
    add_column :clients, :neighbohod, :string
    add_column :clients, :country, :string
  end
end
