class CreateRescuetypes < ActiveRecord::Migration[5.1]
  def change
    create_table :rescuetypes do |t|
      t.string :label
      t.integer :status

      t.timestamps
    end
  end
end
