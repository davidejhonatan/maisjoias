class CreatePoints < ActiveRecord::Migration[5.1]
  def change
    create_table :points do |t|
      t.references :tree, foreign_key: true
      t.references :sell, foreign_key: true
      t.integer :points
      t.integer :status

      t.timestamps
    end
  end
end
