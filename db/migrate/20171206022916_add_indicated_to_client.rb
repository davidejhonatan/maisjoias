class AddIndicatedToClient < ActiveRecord::Migration[5.1]
  def change
    add_column :clients, :indicated, :integer
  end
end
