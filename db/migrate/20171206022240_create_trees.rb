class CreateTrees < ActiveRecord::Migration[5.1]
  def change
    create_table :trees do |t|
      t.integer :father
      t.integer :child
      t.integer :level
      t.integer :status
      t.integer :client_id
      t.timestamps
    end
  end
end
