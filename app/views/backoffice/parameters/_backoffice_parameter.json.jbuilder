json.extract! backoffice_parameter, :id, :created_at, :updated_at
json.url backoffice_parameter_url(backoffice_parameter, format: :json)
