json.extract! backoffice_sell, :id, :client_id, :value_cents, :code, :description, :created_at, :updated_at
json.url backoffice_sell_url(backoffice_sell, format: :json)
