module ApplicationHelper

	ESTADOS_BRASILEIROS = [
    ["Acre", "AC"],
    ["Alagoas", "AL"],
    ["Amapá", "AP"],
    ["Amazonas", "AM"],
    ["Bahia", "BA"],
    ["Ceará", "CE"],
    ["Distrito Federal", "DF"],
    ["Espírito Santo", "ES"],
    ["Goiás", "GO"],
    ["Maranhão", "MA"],
    ["Mato Grosso", "MT"],
    ["Mato Grosso do Sul", "MS"],
    ["Minas Gerais", "MG"],
    ["Pará", "PA"],
    ["Paraíba", "PB"],
    ["Paraná", "PR"],
    ["Pernambuco", "PE"],
    ["Piauí", "PI"],
    ["Rio de Janeiro", "RJ"],
    ["Rio Grande do Norte", "RN"],
    ["Rio Grande do Sul", "RS"],
    ["Rondônia", "RO"],
    ["Roraima", "RR"],
    ["Santa Catarina", "SC"],
    ["São Paulo", "SP"],
    ["Sergipe", "SE"],
    ["Tocantins", "TO"]
  ]

    STATUS = [
    ["Ativo", "active"],
    ["Inativo", "inactive"]
    ]

    TIPOSCONTAS = [
    ["Conta Corrente", "Conta Corrente"],
    ["Poupança", "Poupança"]
    ]
    
    RESCUED = [
      ["Dinheiro" ,"1"],
      ["Produtos","2"]
    ]

  def options_for_states(selected)
    options_for_select(ESTADOS_BRASILEIROS, selected)
  end

  def options_for_status(selected)
    options_for_select(STATUS, selected)
  end

  def options_for_acc_type(selected)
    options_for_select(TIPOSCONTAS, selected)
  end

  def options_for_rescue(selected)
    options_for_select(RESCUED,selected)
  end


end
