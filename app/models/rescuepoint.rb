class Rescuepoint < ApplicationRecord


  scope :search_client, -> client_id { where(:client_id => client_id)}
  scope :search_created_at, -> started_at, ended_at { where(:rescue_date => started_at..ended_at) }
  scope :search_branch, -> branch_id { where(:branch_id => branch_id)}
  scope :active, -> {where(:status => 0)}



  enum rescued: {:no => 0, :yes => 1}
  
    scope :with_rescued, -> {where(rescued: 1)}
    scope :with_no_rescued, -> {where(rescued: 0)}
  belongs_to :client
  belongs_to :rescuetype
  belongs_to :branch
  has_one :voucher
  attr_accessor  :value_venda


end
