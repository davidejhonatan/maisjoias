class Voucher < ApplicationRecord
  enum status: {:'Liberado/Aguardando Pagamento' => 0, :'Usado/Pagamento Realizado' => 1}
  scope :with_rescued, -> {where(status: 1)}
  scope :with_no_rescued, -> {where(status: 0)}  
  belongs_to :rescuepoint
  validates :code, uniqueness: true
end
