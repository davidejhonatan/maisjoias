class Tree < ApplicationRecord
    belongs_to :client_father, foreign_key: "father", class_name: 'Client' 
    belongs_to :client_child, foreign_key: "child", class_name: 'Client'
    has_many :point
end
