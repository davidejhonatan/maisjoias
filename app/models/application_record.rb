class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def status_br
  if self.status == 'active'
		'Ativo'
	else
		'Inativo'
	end
  end
  def rescued_br
    if self.rescued == 'yes'
      'Resgatado'
    else
      'Aguardando'
    end
  end
  def sell_type_br
    if self.value_cents > 0
      "Venda"
    else
      "Devolução"
    end
  end
end
