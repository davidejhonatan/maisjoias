class Sell < ApplicationRecord
  scope :search_client, -> client_id { where(:client_id => client_id)}
  scope :search_created_at, -> started_at, ended_at { where(:sell_date => started_at..ended_at) }
  scope :search_branch, -> branch_id { where(:branch_id => branch_id)}
  scope :active, -> {where(:status => 0)}
  
	enum status: {:active => 0, :inactive => 1}

	scope :with_active, -> {where(status: 0)}
	scope :with_inactive, -> {where(status: 1)}

  belongs_to :client
  belongs_to :branch, optional: true
 monetize :value_cents
  validates :value, numericality: { greater_than: -99999999999999999 }
 #Validates

attr_accessor  :value_venda

 validates :client_id, :code, :sell_date, presence: true

def client_name
  client.try(:name, :id)
end

def client_name=(name)
  self.client = Client.find_by(name: name) if name.present?
end

end
