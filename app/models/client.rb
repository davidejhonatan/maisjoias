class Client < ApplicationRecord
	enum status: {:active => 0, :inactive => 1}

	scope :with_active, -> {where(status: 0)}
	scope :with_inactive, -> {where(status: 1)}

	has_many :phones
	belongs_to :branch
	has_many :sells
	belongs_to :bank
	belongs_to :client, foreign_key: "indicated", optional: true
	
	#validates :cpf, :rg, uniqueness: true

	def client_name
		indicated.try(:name, :id)
	end
	  
	def client_name=(name)
		self.indicated = Client.find_by(name: name).id if name.present?
		
	end

	accepts_nested_attributes_for :phones, :bank, allow_destroy: true

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end