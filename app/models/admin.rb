class Admin < ApplicationRecord
	enum status: {:active => 0, :inactive => 1}

  scope :with_active, -> {where(status: 0)}
  scope :with_inactive, -> {where(status: 1)}


	belongs_to :branch

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

end
