class Branch < ApplicationRecord
  enum status: {:active => 0, :inactive => 1}

  scope :with_active, -> {where(status: 0)}
  scope :with_inactive, -> {where(status: 1)}

  has_many :clients
  has_many :admins
  has_many :sells
  has_many :recuepoint
end
