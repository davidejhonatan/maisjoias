//= require bootstrap_sb_admin_base_v2
//= require bootbox
//= require jquery-ui/widgets/datepicker
//= require jquery-ui/i18n/datepicker-pt-BR



/* Bootbox - Sobrescreve o data-confirm do Rails */
$.rails.allowAction = function(element) {
  var message = element.attr('data-confirm');
  if (!message) { return true; }

  var opts = {
    title: "Confirmação",
    message: message,
    buttons: {
        confirm: {
            label: 'Sim',
            className: 'btn-success'
        },
        cancel: {
            label: 'Não',
            className: 'btn-danger'
        }
    },
    callback: function(result) {
      if (result) {
        element.removeAttr('data-confirm');
        element.trigger('click.rails')
      }
    }
  };

  bootbox.confirm(opts);

  return false;
}

/* Checkbox da página de vendas */
  $(document).ready(function(){
    $('#checkbox1').change(function(){
      $('#status1')/*.html('Toggle: ' + $(this).prop('checked'))*/
      if(this.checked)
        $('#formvendas').fadeIn('slow');
      else
        $('#formvendas').fadeOut('slow');

    });

      $('#checkbox2').change(function(){
      $('#status2')/*.html('Toggle: ' + $(this).prop('checked'))*/
      if(this.checked)
        $('#formdev').fadeIn('slow');
      else
        $('#formdev').fadeOut('slow');

    });

      $('#checkbox3').change(function(){
      $('#status3')/*.html('Toggle: ' + $(this).prop('checked'))*/
      if(this.checked)
        $('#formvendasdev').fadeIn('slow');
      else
        $('#formvendasdev').fadeOut('slow');

    });

  });






/* Campo de data */
$( function() {
    $( "#datepicker1" ).datepicker();
    $( "#datepicker2" ).datepicker();
    $( "#datepicker3" ).datepicker();
  } );

var corCompleta = "#99ff8f"
var corIncompleta = "#eff70b"

function ResetCampos(){
    var textFields = document.getElementsByTagName("input");
        for(var i=0; i < textFields.length; i++){
        if(textFields[i].type == "text"){
            textFields[i].style.backgroundColor = "";
            textFields[i].style.borderColor = "";
        }
    }
}

/* Máscaras dos campos de formulários */
function coresMask(t){
	var l = t.value;
	var m = l.length;
	var x = t.maxLength;
	if(m==0){
		t.style.borderColor="";
		t.style.backgroundColor="";
	}
	else if(m<x){
		t.style.borderColor=corIncompleta;
		t.style.backgroundColor=corIncompleta;
	}else{
		t.style.borderColor=corCompleta;
		t.style.backgroundColor=corCompleta;
	}
}

function mascara(m,t,e,c){
	var cursor = t.selectionStart;
	var texto = t.value;
	texto = texto.replace(/\D/g,'');
	var l = texto.length;
	var lm = m.length;
	if(window.event) {
	    id = e.keyCode;
	} else if(e.which){
	    id = e.which;
	}
	cursorfixo=false;
	if(cursor < l)cursorfixo=true;
	var livre = false;
	if(id == 16 || id == 19 || (id >= 33 && id <= 40))livre = true;
 	ii=0;
 	mm=0;
 	if(!livre){
	 	if(id!=8){
		 	t.value="";
		 	j=0;
		 	for(i=0;i<lm;i++){
		 		if(m.substr(i,1)=="#"){
		 			t.value+=texto.substr(j,1);
		 			j++;
		 		}else if(m.substr(i,1)!="#"){
		 			t.value+=m.substr(i,1);
		 		}
		 		if(id!=8 && !cursorfixo)cursor++;
		 		if((j)==l+1)break;

		 	}
	 	}
	 	if(c)coresMask(t);
 	}
 	if(cursorfixo && !livre)cursor--;
 	t.setSelectionRange(cursor, cursor);
}

/* Layout do perfil show client */
$(function() {
  $('#profile-image1').on('click', function() {
    $('#profile-image-upload').click();
  });
});
