class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  layout :layout_by_resource

  private

  def layout_by_resource
	  if devise_controller? && resource_name == :admin
	    "backoffice_devise"
	  elsif
	   devise_controller? && resource_name == :client
	  		"panel_devise"
	  else
	    "application"
	  end
	end

end
