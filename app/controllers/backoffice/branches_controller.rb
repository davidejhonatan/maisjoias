class Backoffice::BranchesController < BackofficeController
  before_action :set_branch, only: [:edit, :update, :destroy]

  def index
    @contador = Branch.all
    @admins = Admin.all
    @clients = Client.all
  	@branches = Branch.order(:name).page(params[:page]).per(15)
  end

  def new
    @branch = Branch.new

  end

  def create
  	@branch= Branch.new(params_branch)
    if @branch.save
      redirect_to backoffice_branches_path, notice: "A cadastro (#{@branch.name}) foi cadastrada com sucesso!"
    else
      render :new
    end
  end

  def edit
    #usei o before_action para rodar a função set_branch
  end

  def destroy
    branch_name = @branch.name

    if @branch.destroy
      redirect_to backoffice_branches_path, notice: "A cadastro (#{branch_name}) foi excluido com sucesso!"
    else
      render :index
    end

  end

  def update
    #usei o before_action para rodar a função set_branch
    if @branch.update(params_branch)
      redirect_to backoffice_branches_path, notice: "O cadastro (#{@branch.name}) foi atualizado com sucesso!"
    else
      render :edit
    end
  end

  private
    def set_branch
      @branch = Branch.find(params[:id])
    end

    def params_branch
      params.require(:branch).permit(:name, :city, :address, :neighbohod, :state, :status)
    end

end




