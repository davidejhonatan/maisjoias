class Backoffice::ClientsController < BackofficeController
  before_action :set_client, only: [:edit, :update, :destroy, :show]

  def index
    @contador = Client.where(:branch_id => current_admin.branch_id)
    @contativo = 0
    @continativo = 0
    @contador.each do |client|
      if client.status_br == "Ativo"
          @contativo = @contativo + 1
      elsif client.status_br == "Inativo"
          @continativo = @continativo + 1
      end
    end

    @indicateds = Client.order(:name).where("name like ? and status = ?", "%#{params[:term]}%", 0)
    
    respond_to do |format|
      format.html
      format.json {
        render json: @indicateds.map(&:name).to_json
      }
    end

  	@clients = Client.where(:branch_id => current_admin.branch_id).order(:name).page(params[:page]).per(15)
    @branches = Branch.all
  end

  def new
    @client = Client.new
    @client.build_bank

  end

  def create
    @client = Client.new(params_client)
    @client.branch_id = current_admin.branch_id
    
    if @client.save
      params_twig = {:father => @client.indicated, :child => @client.id, :level => 2, :status => 0, :created_at => Time.now, :updated_at => Time.now}
      @twig = Tree.new(params_twig)
      @twig.save
      params_twig2 = {:father => @client.id, :child => @client.id, :level => 1, :status => 0, :created_at => Time.now, :updated_at => Time.now}
      @twig2 = Tree.new(params_twig2)
      @twig2.save
      search = @client.indicated
      find_twig = Tree.where("child = ? AND father != ?",search,search)
      find_twig.each do |new_twig|
        new_level = new_twig.level+1
        if new_level>4
          new_twig_params = {:father => new_twig.father, :child => @client.id, :level => new_level,:status => 1, :created_at => Time.now, :updated_at => Time.now}
        else
          new_twig_params = {:father => new_twig.father, :child => @client.id, :level => new_level,:status => 0, :created_at => Time.now, :updated_at => Time.now}
        end
        create_twig = Tree.new(new_twig_params)
        create_twig.save
      end
      redirect_to backoffice_clients_path, notice: "O cadastro (#{@client.email}) foi cadastrada com sucesso!"
    else
      render :new
    end
  end

  def edit
    #usei o before_action para rodar a função set_client
  end

  def destroy
    client_email = @client.email

    if @client.destroy
      redirect_to backoffice_clients_path, notice: "O cadastro (#{client_email}) foi excluido com sucesso!"
    else
      render :index
    end

  end

  def update
    passwd = params[:client][:password]
    passwd_confirmation = params[:client][:password_confirmation]
    if passwd.blank? && passwd_confirmation.blank?
      params[:client].delete(:password)
      params[:client].delete(:password_confirmation)
    end


    #usei o before_action para rodar a função set_client
    if @client.update(params_client)
      redirect_to backoffice_clients_path, notice: "O cadastro (#{@client.email}) foi atualizado com sucesso!"
    else
      render :edit
    end
  end

  private
    def set_client
      @client = Client.find(params[:id])
    end

    def params_client
      params.require(:client).permit(:client_name, :email, :password, :password_confirmation,
                                     :name, :state, :cpf, :rg, :address, :neighbohod,
                                     :country, :status, :city, :postal_code, :complement,
                                     :birthdate, phones_attributes:[:phone,:_destroy,:id],
                                      bank_attributes:[:bank_name, :bank_account_type, :bank_agency,
                                     :bank_account, :bank_variance])
    end


end
