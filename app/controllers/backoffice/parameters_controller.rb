class Backoffice::ParametersController < BackofficeController
  before_action :set_parameter, only: [:edit, :update, :destroy]

  def index
    @contador = Parameter.all
  	@parameters = Parameter.order(:label).page(params[:page]).per(15)
  end

  def new
    @parameter = Parameter.new

  end

  def create
    @parameter = Parameter.new(params_parameter)

    if @parameter.save
      redirect_to backoffice_parameters_path, notice: "O cadastro (#{@parameter.label}) foi cadastrada com sucesso!"
    else
      render :new
    end
  end

  def edit
    #usei o before_action para rodar a função set_parameter
  end

  def destroy

    if @parameter.destroy
      redirect_to backoffice_parameters_path, notice: "O cadastro (#{parameter_email}) foi excluido com sucesso!"
    else
      render :index
    end

  end

  def update
    
    if @parameter.update(params_params)
      redirect_to backoffice_parameters_path, notice: "O cadastro (#{@parameter.name}) foi atualizado com sucesso!"
    else
      render :edit
    end
  end

  private
  def set_parameter
    @parameter = Parameter.find(params[:id])
  end

  def params_params
    params.require(:parameter).permit(:name, :value)
  end

end
