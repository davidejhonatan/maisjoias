class Backoffice::SellsController < BackofficeController
  before_action :set_sell, only: [:edit, :update, :destroy]
  has_scope :search_client
  has_scope :search_created_at,:using => [:start_period, :end_period]
  has_scope :search_branch
  has_scope :active, :type => :boolean
  def index
    if current_admin.id==2
      @sells = apply_scopes(Sell).all
    else
      @sells = apply_scopes(Sell).all
    end
    @contador = @sells    
    @branchs = Branch.all
    @clients = Client.all

    respond_to do |format|
      format.html
      format.json {
        render json: @clients.map(&:name).to_json
      }
    end

  end


  def new
    @sell = Sell.new
    options_for_select
  end

  def create

    @sell = Sell.new(params_sell)
    @sell.branch_id = current_admin.branch_id
    if params_sell[:sell_type] == "formdev"
      @sell.value = @sell.value * -1
    elsif params_sell[:sell_type] == "formvendasdev"
      @sell.value =params_sell[:value_venda]
    else
      @sell.value = @sell.value
    end


    if @sell.save
      if @sell.value>0 # só salva os pontos se for venda
        #salvar pontos da compra para o cliente que realizou a compra
        twig = Tree.where("father = ? and child = ? and status = 0", @sell.client_id,@sell.client_id) #pega o id da tree do cliente para os pontos dele mesmo
        level_value = Parameter.where(:label => "value_nivel_1", :status => 0)[0].value #pega o valor do nivel 1
        calculate_points = (@sell.value.fractional*level_value.to_f/100)/100 # calcula o valor em pontos
        new_points_param = {:tree_id => twig[0].id, :sell_id => @sell.id, :points => calculate_points, :status => 0, :created_at => Time.now, :updated_at => Time.now}
        add_points = Point.new(new_points_param) #cria novo Point com os paramêtros
        add_points.save # salva ¬¬
        # atualizar pontos do cliente
        client_father = Client.find(@sell.client_id)
        points_client = client_father.total_points.to_i+calculate_points
        available_points_client = client_father.available_points.to_i+calculate_points
        update_client = {:total_points => points_client, :available_points => available_points_client}
        client_father.update(update_client)
        # salvar pontos da compra para os niveis superiores do cliente que realizou a compra
        twigs = Tree.where("child = ? and status = 0 and father != ?", @sell.client_id,@sell.client_id) # pega todas as trees que contem o cliente como filho
        twigs.each do |new_twig| # loop pelas tree
          level_value = Parameter.where(:label => "value_nivel_"+new_twig.level.to_s, :status => 0)[0].value #pega o paramentro baseado no level da tree
          calculate_points = (@sell.value.fractional*level_value.to_f/100)/100 #calcular pontos
          new_points_param = {:tree_id => new_twig.id, :sell_id => @sell.id, :points => calculate_points, :status => 0, :created_at => Time.now, :updated_at => Time.now }
          add_points = Point.new(new_points_param) #cria novo Point com os paramêtros
          add_points.save # salva ¬¬
          #atualizar pontos do cliente pai
          client_father = Client.find(new_twig.father) # pega o cliente pai
          points_client = client_father.total_points.to_i+calculate_points
          available_points_client = client_father.available_points.to_i+calculate_points
          update_client = {:total_points => points_client, :available_points => available_points_client}
          client_father.update(update_client) #atualizando cliente pai
        end
      end
      if params_sell[:sell_type] == "formvendasdev"
        @sell2 = Sell.new(params_sell)
        @sell2.value = @sell2.value * -1
        @sell2.branch_id = current_admin.branch_id
    
        @sell2.created_at = Date.current
        if @sell2.save
          redirect_to backoffice_sells_path, notice: "A cadastro (#{@sell.code}) foi cadastrada com sucesso!"
        end
      else
        redirect_to backoffice_sells_path, notice: "A cadastro (#{@sell.code}) foi cadastrada com sucesso!"
      end
    else
      render :index
    end
  end

  def edit
    options_for_select
    #usei o before_action para rodar a função set_sell
  end

  def destroy
    sell_code = @sell.code

    if @sell.destroy
      redirect_to backoffice_sells_path, notice: "A cadastro (#{sell_code}) foi excluido com sucesso!"
    else
      render :index
    end

  end

  def update
    #usei o before_action para rodar a função set_sell
    if @sell.update(params_sell)
      redirect_to backoffice_sells_path, notice: "O cadastro (#{@sell.code}) foi atualizado com sucesso!"
    else
      render :edit
    end
  end

  private
    def options_for_select
      @client_options_for_select = Client.all
    end

    def set_sell
      @sell = Sell.find(params[:id])
    end

    def params_sell
      params.require(:sell).permit(:client_name, :sell_type, :value, :code, :description, :sell_date, :value_venda, :status)
    end


end
