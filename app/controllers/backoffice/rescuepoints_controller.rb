class Backoffice::RescuepointsController < BackofficeController
  before_action :set_rescuepoint, only: [:show, :edit, :update, :destroy]

  has_scope :search_client
  has_scope :search_created_at,:using => [:start_period, :end_period]
  has_scope :search_branch
  has_scope :active, :type => :boolean

  # GET /rescuepoints
  # GET /rescuepoints.json
  def index
    #@rescuepoints = Rescuepoint.order(:client_id).page(params[:page]).per(15)
    if current_admin.id==2
      @rescuepoints = apply_scopes(Rescuepoint).all
    else
      @rescuepoints = apply_scopes(Rescuepoint).all
    end
    @contador = @rescuepoints
    @branchs = Branch.all
    @clients = Client.all

    

  end

  # GET /rescuepoints/1
  # GET /rescuepoints/1.json
  def show
  end

  # GET /rescuepoints/new
  def new
    @rescuepoint = Rescuepoint.new
  end

  # GET /rescuepoints/1/edit
  def edit
  end

  # POST /rescuepoints
  # POST /rescuepoints.json
  def create
    @rescuepoint = Rescuepoint.new(rescuepoint_params)

    respond_to do |format|
      if @rescuepoint.save
        format.html { redirect_to @rescuepoint, notice: 'Rescuepoint was successfully created.' }
        format.json { render :show, status: :created, location: @rescuepoint }
      else
        format.html { render :new }
        format.json { render json: @rescuepoint.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rescuepoints/1
  # PATCH/PUT /rescuepoints/1.json
  def update
    respond_to do |format|
      
      if @rescuepoint.update({:rescued => 1})
        if rescuepoint_params[:voucher_id]!=""
          @voucher = Voucher.find(rescuepoint_params[:voucher_id])
          att_voucher = {:status => 1}
          if @voucher.update(att_voucher)
          end
        end
        format.html { redirect_to rescuepoints_url, notice: 'Rescuepoint was successfully updated.' }
        format.json { render :show, status: :ok, location: @rescuepoint }
      else
        format.html { render :edit }
        format.json { render json: @rescuepoint.errors, status: :unprocessable_entity }
      end
    end
  end

  

  # DELETE /rescuepoints/1
  # DELETE /rescuepoints/1.json
  def destroy
    @rescuepoint.destroy
    respond_to do |format|
      format.html { redirect_to rescuepoints_url, notice: 'Rescuepoint was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rescuepoint
      @rescuepoint = Rescuepoint.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rescuepoint_params
      #params.fetch(:rescuepoint, {})
      params.require(:rescuepoint).permit(:rescued, :voucher_id)
    end
end
