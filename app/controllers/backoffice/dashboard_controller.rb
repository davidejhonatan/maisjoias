class Backoffice::DashboardController < BackofficeController
  def index
    now = Time.now
    before_1_month = now -2419200
    if current_admin.id==2
      @clients = Client.where(:status => 0)
      @clients_last_month = Client.where(:status => 0, :created_at => before_1_month..now)
      @sells_last_month = Sell.where(:created_at => before_1_month..now)
      @rescued_last_month = Rescuepoint.where(:created_at => before_1_month..now)
    else
      @clients = Client.where(:status => 0, :branch_id => current_admin.branch_id )
      @clients_last_month = Client.where(:status => 0, :created_at => before_1_month..now, :branch_id => current_admin.branch_id)
      @sells_last_month = Sell.where(:created_at => before_1_month..now, :branch_id => current_admin.branch_id)
      @rescued_last_month = Rescuepoint.where(:created_at => before_1_month..now, :branch_id => current_admin.branch_id)
    end

    
  end
end
