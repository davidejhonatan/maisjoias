class Panel::DashboardController < PanelController
  def index
    now = Time.now
    before_1_month = now -2419200
    @sells = Sell.where(:sell_date => before_1_month..now, :status => 0)
    @indicateds = Tree.where(:father => current_client.id, :status => 0, :created_at => before_1_month..now)
    @rescueds = Rescuepoint.where(:client_id => current_client.id, :created_at => before_1_month..now)
    @client = Client.find(current_client.id)
  end
end
