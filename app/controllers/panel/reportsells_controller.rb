class Panel::ReportsellsController < PanelController
before_action :set_reportsell, only: [:edit, :update, :destroy, :show]

def index
  @contador = Sell.all
  @reportsells = Sell.all
  if params[:start_period] and params[:end_period] and params[:start_period]!="" and params[:end_period]!=""
    @reports = Sell.where(:sell_date => params[:start_period]..params[:end_period], :status => 0, :client_id => current_client.id)
  else
    @reports = Sell.where(:status => 0, :client_id => current_client.id)
  end    
end

def new
  @reportsell = Sell.new

end

def create
  @reportsell= Sell.new(params_reportsell)
  if @reportsell.save
    redirect_to panel_reportsells_path, notice: "A cadastro (#{@reportsell.name}) foi cadastrada com sucesso!"
  else
    render :new
  end
end

def edit
  #usei o before_action para rodar a função set_reportsell
end

def destroy
  reportsell_name = @reportsell.name

  if @reportsell.destroy
    redirect_to panel_reportsells_path, notice: "A cadastro (#{reportsell_name}) foi excluido com sucesso!"
  else
    render :index
  end

end

def update
  #usei o before_action para rodar a função set_reportsell
  if @reportsell.update(params_reportsell)
    redirect_to panel_reportsells_path, notice: "O cadastro (#{@reportsell.name}) foi atualizado com sucesso!"
  else
    render :edit
  end
end

private
  def set_reportsell
    @reportsell = Sell.find(params[:id])
  end

  def params_reportsell
    params.require(:reportsell).permit(:name, :city, :address, :neighbohod, :state, :status)
  end

end




  
    