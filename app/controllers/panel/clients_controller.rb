class Panel::ClientsController < PanelController
  before_action :set_client, only: [:edit, :update, :destroy]

  def index
  	@clients = Client.all
  end

  def new
    @client = Client.new
  end

  def create
    @client = Client.new(params_client)
    if @client.save
      redirect_to panel_clients_path, notice: "A cadastro (#{@client.email}) foi cadastrada com sucesso!"
    else
      render :new
    end
  end

  def edit
    #usei o before_action para rodar a função set_client
  end

  def destroy
    client_email = @client.email

    if @client.destroy
      redirect_to panel_clients_path, notice: "A cadastro (#{client_email}) foi excluido com sucesso!"
    else
      render :index
    end

  end

  def update
    passwd = params[:client][:password]
    passwd_confirmation = params[:client][:password_confirmation]
    if passwd.blank? && passwd_confirmation.blank?
      params[:client].delete(:password)
      params[:client].delete(:password_confirmation)
    end


    #usei o before_action para rodar a função set_client
    if @client.update(params_client)
      redirect_to panel_clients_path, notice: "O cadastro (#{@client.email}) foi atualizado com sucesso!"
    else
      render :edit
    end
  end

  private
    def set_client
      @client = Client.find(params[:id])
    end

    def params_client
      params.require(:client).permit(:email, :password, :password_confirmation, :name, :role, :cpf, :rg)
    end


end
