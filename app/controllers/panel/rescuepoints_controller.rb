class Panel::RescuepointsController < PanelController
    before_action :set_client, only: [:edit, :update, :destroy]
    
    def index
        @rescuepoints = Rescuepoint.where(client_id: current_client.id).order(:client_id).page(params[:page]).per(15)
    end
       # GET /rescuepoints/1
    # GET /rescuepoints/1.json
    def show
        @dado = Rescuepoint.where(:client_id => current_client.id)
        @voucher = @dado.find(params[:id])
        respond_to do |format|
            format.html
            format.pdf do
                render pdf: "rescuepoints",
                layout: 'pdf'
            end
        end
    end

    # GET /rescuepoints/new
    def new
        @rescuepoint = Rescuepoint.new({client_id: current_client.id})
    end

    # GET /rescuepoints/1/edit
    def edit
    end

    # POST /rescuepoints
    # POST /rescuepoints.json
    def create
        @rescuepoint = Rescuepoint.new(rescuepoint_params)
        @rescuepoint.client_id = current_client.id
        @client = Client.find(current_client.id)
        if rescuepoint_params[:points].to_i <= @client.available_points
            respond_to do |format|
            att_client = {:available_points => @client.available_points - rescuepoint_params[:points].to_i}
            
            if @rescuepoint.save
                if @rescuepoint.rescuetype_id==2
                    @params_final = Parameter.where(:label => 'points_to_product')
                else
                    @params_final = Parameter.where(:label => 'points_to_money')
                end
                params_voucher = {:code => SecureRandom.hex(6),:value => @rescuepoint.points.to_i*@params_final[0].value.to_i, :status => 0, :rescuepoint_id => @rescuepoint.id}
                @voucher = Voucher.new(params_voucher)
                if @voucher.save
                    if @client.update(att_client)
                        format.html { redirect_to panel_rescuepoints_path, notice: 'Rescuepoint was successfully created.' }
                        format.json { render :show, status: :created, location: @rescuepoint }
                    end
                end
            else
                format.html { redirect_to :new_panel_rescuepoint_path }
                format.json { render json: @rescuepoint.errors, status: :unprocessable_entity }
            end
            end
        else
            redirect_to new_panel_rescuepoint_path, alert: "Você não tem (#{rescuepoint_params[:points]}) pontos para realizar esse saque."            
        end
    end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_rescuepoint
    @rescuepoint = Rescuepoint.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def rescuepoint_params
    #params.fetch(:rescuepoint, {})
    params.require(:rescuepoint).permit(:points, :rescuetype_id)
  end
end
