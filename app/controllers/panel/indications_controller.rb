class Panel::IndicationsController < PanelController
before_action :set_indication, only: [:edit, :update, :destroy, :show]

def index
  
  if params[:start_period]!="" and params[:end_period]!="" and params[:start_period]
    @indics = Tree.where(:created_at => params[:start_period]..params[:end_period],:father => current_client.id, :status => 0)
  elsif params[:start_period]!="" and params[:end_period]!="" and params[:level]!=0 and params[:start_period] and params[:level]
    @indics = Tree.where(:created_at => params[:start_period]..params[:end_period],:father => current_client.id, :status => 0, :level => params[:level])
  elsif params[:level]!="0" and params[:level]
    @indics = Tree.where(:father => current_client.id, :status => 0, :level => params[:level])
  else
    @indics = Tree.where(:father => current_client.id, :status => 0)
  end
  @levels = { 'Todos' => 0,'Nível 1' =>1, 'Nível 2'=>2,'Nível 3' =>3,'Nível 4' =>4}
end

def new
  @indication = Tree.new

end

def create
  @indication= Tree.new(params_indication)
  if @indication.save
    redirect_to panel_indications_path, notice: "A cadastro (#{@indication.name}) foi cadastrada com sucesso!"
  else
    render :new
  end
end

def edit
  #usei o before_action para rodar a função set_indication
end

def destroy
  indication_name = @indication.name

  if @indication.destroy
    redirect_to panel_indications_path, notice: "A cadastro (#{@indications.id}) foi excluido com sucesso!"
  else
    render :index
  end

end

def update
  #usei o before_action para rodar a função set_indication
  if @indication.update(params_indication)
    redirect_to panel_indications_path, notice: "O cadastro (#{@indications.id}) foi atualizado com sucesso!"
  else
    render :edit
  end
end

private
  def set_indication
    @indication = Tree.find(params[:id])
  end

  def params_indication
    params.require(:indication).permit(:father, :child, :level)
  end

end




  
    