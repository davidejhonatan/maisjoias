Rails.application.routes.draw do
  
    get 'backoffice', to: 'backoffice/dashboard#index'
    get 'admin', to: 'backoffice/dashboard#index'
    get 'filiais', to: 'backoffice/branches#index'
    get 'vendas', to: 'backoffice/sells#index'
    get 'parameters', to: 'backoffice/parameters#index'
    get 'panel', to: 'panel/dashboard#index'
    get 'cliente', to: 'panel/dashboard#index'
    get 'rescuepoints', to: 'backoffice/rescuepoints#index'
    get 'clientevendas', to: 'panel/reportseels#index'
    get 'indications', to: 'panel/indications#index'
    
  namespace :backoffice do
    resources :admins, except: [:show]
    resources :clients
    resources :branches
    resources :sells
    resources :parameters
    resources :rescuepoints
    get 'dashboard', to: 'dashboard#index'
    get 'clients', to: 'clients#index'
    get 'branches', to: 'branches#index'
    get 'sells', to: 'sells#index'
    get 'parameters', to: 'parameters#index'
    get 'rescuepoints', to: 'rescuepoints#index'
  end

  namespace :panel do
    resources :clients, except: [:show]
    resources :rescuepoints
    resources :reportsells
    resources :indications
    get 'dashboard', to: 'dashboard#index'
    get 'reportseels', to: 'reportsells#index'
    get 'search', to: 'reportsells#search'
    get 'indications', to: 'indications#index'
  end

  namespace :site do
    get 'home', to: 'home#index'
  end


  devise_for :clients, :skip => [:registrations]
  devise_for :admins, :skip => [:registrations]

  root 'site/home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
