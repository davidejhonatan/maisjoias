namespace :utils do
  desc "CRIA ADMINS FAKE"
  task generate_admins: :environment do
  	puts "Cadastrando branches FAKE..."
  	50.times do
  		Branch.create!(name: Faker::Company.name, city: Faker::Address.city, address: Faker::Address.street_address, neighbohod: Faker::Address.community, state: Faker::Address.state, status: "Ativo")
  	end
  	puts "Branches FAKE cadastrados!"
  	puts "Cadastrando admins FAKE..."
  	50.times do |i|
  		Admin.create!(email: Faker::Internet.email, password: "123456", password_confirmation: "123456", name: Faker::Name.name, cpf: CpfUtils.cpf_formatted,
  					  rg: "002689292", status: "Ativo",
  					  branch_id: i+1)
  	end
  	puts "Admins FAKE cadastrados!"
  end

end
